The built in LabVIEW Tab Control has two main drawbacks: You must specify the tabs at design time, and it doesn't play nicely with resizing panels. The .NET Tab Control does not have these limitations. Again, there are two components

### Tab Control

This is a subpanel that will contain the tab control.

### Tab Panel

This is the actual Panel that will be inserted into the Tab. Similar to the Window Panel type, a Tab Panel must have a title, and the Panel can set it's own title dynamically using the Panel Data's `Title` field. See the Examples included with the .NET Panels Package for more info and sample code.

## Contributing

See [Contributing.md](CONTRIBUTING.md) for information on how to submit pull requests. Bugs can be reported using the repositories issue tracker.

#### _This package is implemented with LabVIEW 2017_
